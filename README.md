# Parallel Programming in Python: Multithreading and Multiprocessing

Material for the threading and multiprocessing in python workshop.

## Objectives
Learning Outcomes for attendees of this workshop:

* Able to understand the differences between multithreading and multiprocessing.
* Know which tasks are better suited for multithreading or multiprocessing
* Able to use threading and multiprocessing packages for appropriately suited tasks.

## Outline
* Overview:
  * [Jupyter Lab IDE & Notebook Basics](notebooks/jupyter_intro.ipynb)
  * [Overview of parallel computing with python](notebooks/overview.ipynb)
* [Threading:](notebooks/threading.ipynb)
  * c-extension thread enabled libraries (numpy & scipy)
  * threading package
* [Multiprocessing:](notebooks/multiproc.ipynb)
  * Process class
  * Pool class
* Other parallel libraries:
  * [pymp -- OpenMP-like functionality for Python](notebooks/pymp.ipynb)

## Running the Notebooks for this Workshop

* Using jupyter-notebooks on midway 
  * [Juypter-lab launch script repo](https://git.rcc.uchicago.edu/jhskone/jupyter-lab)

We will launch a jupyter lab server instance on a compute node and work from 
within this IDE. See the above jupyter-lab launch script repo link for information
on all of the variable assignments one can use with this script. Outside of this
workshop the script can be reused for your own use with some minor modifications.

After cloning this repository to a directory on RCC's midway, you can launch
this script by issuing the following command from within the repo directory 
where the launch script exists: 

```
./launch-jlab.sh
```

